import React from 'react';
import Router from 'react-router';
import { Route, DefaultRoute } from 'react-router';

// wrapper
import App from './components/App';

// pages
import FrontPage from './components/frontPage/FrontPage';



var routes = (
  <Route handler={App}>

    <Route name="/" handler={FrontPage}/>
    
  </Route>
);

Router.run(routes, (Handler, state) => {
  React.render(<Handler {...state}/>, document.getElementById('main'));
});