import React from 'react';
import {Router, Route, Link, RouteHandler} from 'react-router';
import Template from './template/Template';



class App extends React.Component{
	render(){
		return (
      <Template>
        <RouteHandler/>
      </Template>
    );
	}
}

export default App