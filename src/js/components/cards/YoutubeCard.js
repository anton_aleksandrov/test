import React from 'react';

export default class YoutubeCard extends React.Component {
	render() {
		var item = this.props.item,
				channelLink = 'http://www.youtube.com/channel/'.concat(item.snippet.channelId),
				videoLink = 'http://www.youtube.com/watch?v='.concat(item.id.videoId),
				channel = item.snippet.channelTitle,
				image = item.snippet.thumbnails.medium.url,
				description = item.snippet.description;
				console.log(item)
				
		
		return (
			<div className="col s12 m6 l4">
				<div className="card">
					<div className="card-image">
						<a href={videoLink}>
							<img src={image} />
						</a>
					</div>
					<div className="card-content">
						<p>{description}</p>
					</div>
					<div className="card-action">
						<a href={channelLink}>{channel}</a>
					</div>
				</div>
			</div>
		);
	}
}
