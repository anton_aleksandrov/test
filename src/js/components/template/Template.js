import React from 'react';
import Header from './Header'


export default class Template extends React.Component {
	render() {
		return (
			<div>
        <Header />
      	<main>
          <div className="container">
        		{this.props.children}
          </div>
      	</main>
      </div>
		);
	}
}
