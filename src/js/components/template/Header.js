import React from 'react';
import { Router, Link } from 'react-router';
import HeaderSearch from './HeaderSearch';



export default class Header extends React.Component {
	componentDidMount() {
	  // sideNav trigger init
	  $(".button-collapse-hide").sideNav('hide');
	  $(".button-collapse-left").sideNav({
	    edge: 'left'
	  });
	  // headerSearch trigger init
    $('#search-btn').click(function() {
      $('.header-search').removeClass('hide');
      $('#search').focus();
    })
    $('#search-hide').click(function() {
      $('.header-search').delay( 200 ).addClass('hide');
    });
    $('.header-search').focusout(function() {
      setTimeout(function() {
        $('.header-search').addClass('hide');
      }, 200);
    });
  }
	render() {
		return (
			<header>
			  <div className="navbar-fixed">
			    <nav id="navBar" className="top-nav">
			      <div className="nav-wrapper">
			        <a to="/" className="brand-logo center"><i className="mdi-social-whatshot"><span className="hide-on-small-only">ReactJS</span></i></a>
			        <a id="search-btn" className="pointer right marginright"><i className="paddingright paddingleft header-btn mdi-action-search"></i></a>
			        <HeaderSearch/>
			      </div>
			    </nav>
			  </div>
			</header>
		);
	}
}
