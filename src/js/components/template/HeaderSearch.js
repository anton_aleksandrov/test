import React from 'react';
import Actions from '../../actions/Actions';
import Store from '../../stores/Store';


export default class HeaderSearch extends React.Component {
	render() {
		return (
      <form onSubmit={this.searchVideos.bind(this)} className="header-search hide">
        <div className="input-field">
          <input  id="search" type="search" required />
          <input  id="submit" type="submit" />
          <label htmlFor="search"><i className="mdi-action-search"></i></label>
          <i id="search-hide" className="mdi-navigation-close"></i>
        </div>
      </form>
		);
	}
  searchVideos(){
    // console.log('cahnge')
    Actions.testAction($('#search').val())
  }
}
