import React from 'react';
import Store from '../../stores/Store';
import YoutubeCard from '../cards/YoutubeCard';


export default class FrontPage extends React.Component {

	constructor(props) {
    super(props);
    this.state = {cards: null};
  }
  componentDidMount() {
    Store.addChangeListener(this._onChange.bind(this));
  }
  componentWillUnmount() {
    Store.removeChangeListener();
  }
	render() {
		var cardsData = this.state.cards,
				cards = [];

		if (cardsData != null) {
			var items = cardsData.items
			// console.log('cardsData', cardsData.items)
      for (var key in items) {
      	// console.log("items", items[key])
      	
				cards.push(<YoutubeCard key={key} item={items[key]}/>)
			}
		};
		
		return (
			<div className="row">
				{cards}
			</div>
		);
	}
  _onChange(){
    Store.testFunction(this._callbackFunction.bind(this))
    // console.log('this', this)
    
  }
  _callbackFunction (argument) {
    // console.log('argument', argument)
    this.setState({
    	cards: argument
    }) 
  }
}
