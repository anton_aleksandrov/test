// import http from 'superagent';
// import { canUseDOM } from 'react/lib/ExecutionEnvironment';
import Dispatcher from '../dispatcher/dispatcher';
import Constants from '../constants/Constants';

export default {

  testAction(customArgument){
    // console.log('actions ', customArgument)    
    Dispatcher.handleViewAction({
      actionArgument: customArgument,
      actionType: Constants.TEST_ACTION
    })
  }

};
