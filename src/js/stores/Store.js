import EventEmitter from 'eventemitter3';
import Dispatcher from '../dispatcher/dispatcher';
import Constants from '../constants/Constants';
import React from 'react/addons';

var CHANGE_EVENT = "change";

var storedData = {}

function _testFunction (payload) {
  // console.log('payload', payload)
  var request = gapi.client.youtube.search.list({
    part: 'snippet',
    type: 'video',
    q: encodeURIComponent(payload).replace(/%20/g, "+"),
    maxResults: 3,
    order: 'viewCount'
  })
  request.execute(function (response) {
    // console.log(response)
    
    storedData = response;
    AppStore.emitChange();
  })
  
}

var AppStore = React.addons.update(EventEmitter.prototype, {$merge: {
  
  emitChange(){
    this.emit(CHANGE_EVENT);
  },

  addChangeListener(callback){
    this.on(CHANGE_EVENT, callback)
  },

  removeChangeListener(callback){
    this.removeListener(CHANGE_EVENT, callback)
  },

  testFunction(callback) {
    // console.log('StoreNote', storedData)
    // console.log('StoreNote callback', callback)
    
    callback(storedData);
  },


  dispatcherIndex:Dispatcher.register(function(payload){
    var action = payload.action; // this is our action from handleViewAction
    switch(action.actionType){
      case Constants.TEST_ACTION:
        _testFunction(payload.action.actionArgument);
        break;
    }
    AppStore.emitChange();

    return true;
  })
}});


export default AppStore;