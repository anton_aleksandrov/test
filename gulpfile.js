var gulp = require('gulp');
// var browserify = require('gulp-browserify');
var concat = require('gulp-concat');
var webserver = require('gulp-webserver');
var uglifyjs = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var sass = require('gulp-sass');
// var babel = require("gulp-babel");
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');


gulp.task('js', function() {
    browserify({
      entries: 'src/js/main.js',
      extensions: ['.jsx'],
      debug: true
    })
    .transform(babelify)
    .bundle()
    .pipe(source('main.js'))
    // .pipe(uglifyjs())
    .pipe(gulp.dest('dist'))
});

gulp.task('sass', function () {
  gulp.src('src/styles/styles.scss')
      .pipe(sass().on('error', sass.logError))
      // .pipe(uglifycss('styles.css'))
      .pipe(gulp.dest('dist/styles'));
})

gulp.task('assets', function() {
    gulp.src('src/index.html')
        .pipe(gulp.dest('dist'));
    gulp.src('src/assets/**/*.*')
        .pipe(gulp.dest('dist/assets'));
});

gulp.task('font', function() {
    gulp.src('src/font')
      .pipe(gulp.dest('dist/font'))
});

gulp.task('libraries', function() {
    gulp.src('src/libs/*.*')
        .pipe(concat('libs.js'))
        // .pipe(uglifyjs())
        .pipe(gulp.dest('dist/libs'));    
});


gulp.task('default', function() {
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/assets/*.*', ['assets']);
    gulp.watch('src/font/**/*.*', ['font']);
    gulp.watch('src/**/*.scss', ['sass']);
    gulp.watch('src/libs/*.js', ['libraries']);
});

gulp.task('webserver', function() {
  gulp.src('./dist')
    .pipe(webserver({
      livereload: true,
      open: true,
      port: 4444
    }));
});